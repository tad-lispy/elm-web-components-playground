module Benchmark exposing (main)

import Browser
import Browser.Events
import Html exposing (Html)
import Html.Attributes


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { time : Float
    , show : Bool
    , animate : Bool
    }


type Msg
    = Animate Float


type alias Flags =
    ()


init : Flags -> ( Model, Cmd Msg )
init () =
    ( { time = 0
      , show = False
      , animate = True
      }
    , Cmd.none
    )


view : Model -> Html Msg
view model =
    if model.show then
        element

    else
        Html.div [] []


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Animate delta ->
            ( { model
                | time = model.time + delta
                , show = not model.show
                , animate = model.time < 600 * 1000
              }
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.animate then
        Browser.Events.onAnimationFrameDelta Animate

    else
        Sub.none


counter : Html Msg
counter =
    Html.node "elm-program"
        [ Html.Attributes.attribute "module" "Counter"
        ]
        []


element : Html Msg
element =
    Html.node "elm-program"
        [ Html.Attributes.attribute "module" "Element"
        ]
        []
