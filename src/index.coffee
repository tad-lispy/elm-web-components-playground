import { Elm as Main } from "./Main.elm"
import { Elm as Benchmark } from "./Benchmark.elm"
import { Elm as Counter } from "./Counter.elm"
import { Elm as Counter2 } from "./Counter2.elm"
import { Elm as Element } from "./Element.elm"

programs = { Main, Benchmark, Counter, Counter2, Element }

class ElmElement extends HTMLElement
  constructor: () ->
    super ``

  connectedCallback: () ->
    # Setup shadow DOM
    shadow = this.attachShadow mode: 'open'
    wrapper = document.createElement 'div'
    shadow.appendChild(wrapper);

    # Find the module
    module = this.getAttribute "module" ? "Main"
    program = programs[module]?[module]
    if not program? then throw Error "No such module: #{module}"


    # Kick it off
    value = this.getAttribute "value" ? "0"
    @process = program.init node: wrapper, flags: { value }
    @process.ports?.outgoing.subscribe (data) =>
      event = new CustomEvent "outgoing", detail: data
      this.dispatchEvent event

  attributeChangedCallback: (name, oldValue, newValue) ->
    # Note: Sometimes this is called before the constructor. Why? The existential check prevents errors from popping out.
    @process?.ports.incoming.send newValue unless oldValue is newValue

  @observedAttributes: [ "value" ]

customElements.define 'elm-program', ElmElement
