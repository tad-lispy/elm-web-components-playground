module Element exposing (main)

import Browser
import Browser.Events
import Html exposing (Html)
import Html.Attributes


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { time : Float
    }


type Msg
    = Animate Float


type alias Flags =
    ()


init : Flags -> ( Model, Cmd Msg )
init () =
    ( { time = 0
      }
    , Cmd.none
    )


view : Model -> Html Msg
view model =
    model.time
        |> String.fromFloat
        |> Html.text


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Animate delta ->
            ( { model
                | time = model.time + delta
              }
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Browser.Events.onAnimationFrameDelta Animate
