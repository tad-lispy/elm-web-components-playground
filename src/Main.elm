module Main exposing (main)

import Browser
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Json.Decode as Decode exposing (Decoder)


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    { reading : Int
    }


type Msg
    = Reading Int


init : Model
init =
    { reading = 0 }


view : Model -> Html Msg
view model =
    Html.div []
        [ model.reading
            |> String.fromInt
            |> (++) "Last reading: "
            |> Html.text
        , Html.ol
            []
            [ Html.li [] [ counter 5 ]
            , Html.li [] [ counter -122 ]
            , Html.li [] [ counter model.reading ]
            ]
        ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        Reading reading ->
            { model | reading = reading }


counter : Int -> Html Msg
counter value =
    let
        readingDecoder : Decoder Msg
        readingDecoder =
            Decode.field "detail" Decode.int
                |> Decode.map Reading
    in
    Html.node "elm-program"
        [ Html.Attributes.attribute "module" "Counter"
        , Html.Attributes.attribute "value" <| String.fromInt value
        , Html.Events.on "outgoing" readingDecoder
        ]
        []
