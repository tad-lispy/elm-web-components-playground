port module Counter exposing (main)

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)


port outgoing : Int -> Cmd msg


port incoming : (String -> msg) -> Sub msg


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { value : String }


type alias Model =
    { count : Int }


type Msg
    = Increment
    | Decrement
    | Set Int


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { count = flags.value |> String.toInt |> Maybe.withDefault 0 }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Increment ->
            let
                count =
                    model.count + 1
            in
            ( { model | count = count }
            , count |> outgoing
            )

        Decrement ->
            let
                count =
                    model.count - 1
            in
            ( { model | count = count }
            , count |> outgoing
            )

        Set count ->
            ( { model | count = count }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick Increment ] [ text "+1" ]
        , div [] [ text <| String.fromInt model.count ]
        , button [ onClick Decrement ] [ text "-1" ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    incoming (String.toInt >> Maybe.withDefault 0 >> Set)
