# Elm + Web Components Playground 🧪🧫🔬

The idea is to run multiple Elm programs as widgets (components). The `Main` program will display them as elements (`Html msg`) and communicate via attributes and events. 

## Development

Clone and then:

```sh
npm install
npx parcel src/index.html
```
